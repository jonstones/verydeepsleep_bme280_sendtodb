// description https://arduinodiy.wordpress.com/2020/01/21/very-deepsleep-and-energy-saving-on-esp8266-part-2-sending-data-with-http/
long SLEEPTIME = 120e6;

const char* WLAN_SSID = "yourssid";
const char* WLAN_PASSWD = "yourPW";
#include <ESP8266WiFi.h>
WiFiClient clientWiFi;
// For MySQL
String apiKeyValue = "tPmAT5Ab3j7F9";
String sensorName = "BME280";
String sensorLocation = "Garden";
#include <ESP8266HTTPClient.h>
const char* serverName = "http://192.168.1.102/post-esp-data.php";
String httpRequestData;
//---------------------
// The ESP8266 RTC memory is arranged into blocks of 4 bytes. The access methods read and write 4 bytes at a time,
// so the RTC data structure should be padded to a 4-byte multiple.
struct {
  uint32_t crc32;   // 4 bytes
  uint8_t channel;  // 1 byte,   5 in total
  //uint8_t bssid[6]; // 6 bytes, 11 in total
  uint8_t ap_mac[6];
  uint8_t padding;  // 1 byte,  12 in total
} rtcData;
//for static ip
IPAddress ip( 192, 168, 1, 35 );
IPAddress dns( 192, 168, 1, 1 );
IPAddress gateway( 192, 168, 1, 1 );
IPAddress subnet( 255, 255, 255, 0 );
//------------for BME280
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>  // library defaults to 0x77, change that if yr BME is 0x76
#include <Wire.h>  ///check
Adafruit_BME280 bme;



void setup() {
  Serial.begin(74880);
  // disable WiFi
  WiFi.mode( WIFI_OFF );
  WiFi.forceSleepBegin();
  delay( 1 );


  // Try to read WiFi settings from RTC memory
  bool rtcValid = false;
  if ( ESP.rtcUserMemoryRead( 0, (uint32_t*)&rtcData, sizeof( rtcData ) ) ) {
    // Calculate the CRC of what we just read from RTC memory, but skip the first 4 bytes as that's the checksum itself.
    uint32_t crc = calculateCRC32( ((uint8_t*)&rtcData) + 4, sizeof( rtcData ) - 4 );
    if ( crc == rtcData.crc32 ) {
      rtcValid = true;
    }
  }
  //-----------------Do stuff
  Wire.begin();
  bool status = bme.begin(); //bme.begin(address)  0x76 or 0x77
  if (!status) {
    Serial.println("no BME detected");
    delay(1);
  }
  BMEsetup(); // set weathermonitormode
  bme.takeForcedMeasurement();
  // Prepare your HTTP POST request data
  httpRequestData = "api_key=" + apiKeyValue + "&sensor=" + sensorName + "&location=" + sensorLocation + "&value1=" + String(bme.readTemperature()) + "&value2=" + String(bme.readHumidity() / 100.0F) + "&value3=" + String(bme.readPressure()) + "";
  Serial.println("Print request string");
  Serial.println(httpRequestData);
  //----------------


  //Switch Radio back On
  WiFi.forceSleepWake();
  delay( 1 );

  // Disable the WiFi persistence.  The ESP8266 will not load and save WiFi settings in the flash memory.
  WiFi.persistent( false );

  // Bring up the WiFi connection
  WiFi.mode( WIFI_STA );
  WiFi.config( ip, dns, gateway, subnet );
  //-----------replacement for WiFi.begin
  if ( rtcValid ) {
    // The RTC data was good, make a quick connection
    WiFi.begin( WLAN_SSID, WLAN_PASSWD, rtcData.channel, rtcData.ap_mac, true );
  }
  else {
    // The RTC data was not valid, so make a regular connection
    WiFi.begin( WLAN_SSID, WLAN_PASSWD );
  }
  //WiFi.begin( WLAN_SSID, WLAN_PASSWD );
  //------wait for connection
  int retries = 0;
  int wifiStatus = WiFi.status();
  while ( wifiStatus != WL_CONNECTED ) {
    retries++;
    if ( retries == 100 ) {
      // Quick connect is not working, reset WiFi and try regular connection
      WiFi.disconnect();
      delay( 10 );
      WiFi.forceSleepBegin();
      delay( 10 );
      WiFi.forceSleepWake();
      delay( 10 );
      WiFi.begin( WLAN_SSID, WLAN_PASSWD );
    }
    if ( retries == 600 ) {
      // Giving up after 30 seconds and going back to sleep
      WiFi.disconnect( true );
      delay( 1 );
      WiFi.mode( WIFI_OFF );
      ESP.deepSleep( SLEEPTIME, WAKE_RF_DISABLED );

      return; // Not expecting this to be called, the previous call will never return.
    }
    delay( 50 );
    wifiStatus = WiFi.status();
  }
  //----
  Serial.println(" WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  //-----
  // Write current connection info back to RTC
  rtcData.channel = WiFi.channel();
  memcpy( rtcData.ap_mac, WiFi.BSSID(), 6 ); // Copy 6 bytes of BSSID (AP's MAC address)
  rtcData.crc32 = calculateCRC32( ((uint8_t*)&rtcData) + 4, sizeof( rtcData ) - 4 );
  ESP.rtcUserMemoryWrite( 0, (uint32_t*)&rtcData, sizeof( rtcData ) );
  //-------
  //verstuur();//verstuur data
  sql();
  //----and go to sleep
  WiFi.disconnect( true );
  delay( 1 );

  // WAKE_RF_DISABLED to keep the WiFi radio disabled when we wake up
  //  ESP.deepSleep( SLEEPTIME, WAKE_RF_DISABLED );
  ESP.deepSleep(ESP.deepSleepMax(), WAKE_RF_DISABLED );
}

void loop() {
  // put your main code here, to run repeatedly:

}

uint32_t calculateCRC32( const uint8_t *data, size_t length ) {
  uint32_t crc = 0xffffffff;
  while ( length-- ) {
    uint8_t c = *data++;
    for ( uint32_t i = 0x80; i > 0; i >>= 1 ) {
      bool bit = crc & 0x80000000;
      if ( c & i ) {
        bit = !bit;
      }

      crc <<= 1;
      if ( bit ) {
        crc ^= 0x04c11db7;
      }
    }
  }

  return crc;
}





void sql() {
  //Check WiFi connection status
  if (WiFi.status() == WL_CONNECTED) {
    HTTPClient http;

    // Your Domain name with URL path or IP address with path
    http.begin(serverName);// deprecated. 
    //http.begin(espClient,serverName);// voor toekomst?
    // Specify content-type header
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");

    // Prepare your HTTP POST request data
    
    Serial.print("httpRequestData: ");
    Serial.println(httpRequestData);

   
    // Send HTTP POST request
    int httpResponseCode = http.POST(httpRequestData);
    
    if (httpResponseCode > 0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
    }
    else {
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
    }
    // Free resources
    http.end();
  }
  else {
    Serial.println("WiFi Disconnected");
  }

}


// weather monitoring
void BMEsetup() {
  Serial.println("-- Weather Station Scenario --");
  Serial.println("forced mode, 1x temperature / 1x humidity / 1x pressure oversampling,");
  Serial.println("filter off");
  bme.setSampling(Adafruit_BME280::MODE_FORCED,
                  Adafruit_BME280::SAMPLING_X1, // temperature
                  Adafruit_BME280::SAMPLING_X1, // pressure
                  Adafruit_BME280::SAMPLING_X1, // humidity
                  Adafruit_BME280::FILTER_OFF   );

  // suggested rate is 1/60Hz (1m)
  // delayTime = 60000; // in milliseconds
  //t=1+[2xT]+[2xP+0.5]+[2xH+0.5]=1+2+2.5+2.5=13ms
}